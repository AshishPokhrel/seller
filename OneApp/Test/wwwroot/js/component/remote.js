var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import 'whatwg-fetch';
export class Remote {
    static post(url, form, callback, error) {
        let data;
        let token = Remote.getCookieValue('X-CSRF-TOKEN');
        let reqValToken = Remote.getCookieValue('RequestVerificationToken');
        data = JSON.stringify(Remote.serializeToJson(form));
        fetch(url, {
            credentials: 'same-origin', method: 'post', body: JSON.stringify(data), headers: {
                'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'X-CSRF-TOKEN': token, 'RequestVerificationToken': reqValToken
            }
        })
            .then((response) => {
            if (Remote.checkSessionExpired(response))
                return response.json();
            else
                return { error: true, statusText: response.statusText, originalMessage: response };
        })
            .catch((reason) => { Remote.onError(reason, error); })
            .then((result) => {
            if (!result.error)
                callback(result);
            else {
                Remote.onError(result, error);
            }
        });
    }
    static postAsync(url, form) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            let token = Remote.getCookieValue('X-CSRF-TOKEN');
            let reqValToken = Remote.getCookieValue('RequestVerificationToken');
            data = JSON.stringify(Remote.serializeToJson(form));
            return yield fetch(url, {
                credentials: 'same-origin', method: 'post', body: JSON.stringify(data), headers: {
                    'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'X-CSRF-TOKEN': token, 'RequestVerificationToken': reqValToken
                }
            });
        });
    }
    static postData(url, data, callback, error) {
        let token = Remote.getCookieValue('X-CSRF-TOKEN');
        let reqValToken = Remote.getCookieValue('RequestVerificationToken');
        fetch(url, {
            credentials: 'same-origin', method: 'post', body: JSON.stringify(data), headers: {
                'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'X-CSRF-TOKEN': token, 'RequestVerificationToken': reqValToken
            }
        })
            .then((response) => {
            if (Remote.checkSessionExpired(response))
                return response.json();
            else
                return { error: true, statusText: response.statusText, originalMessage: response };
        })
            .catch((reason) => { Remote.onError(reason, error); })
            .then((result) => {
            if (result && !result.error)
                callback(result);
            else {
                Remote.onError(result, error);
            }
        });
    }
    static postDataAsync(url, data) {
        return __awaiter(this, void 0, void 0, function* () {
            let token = Remote.getCookieValue('X-CSRF-TOKEN');
            let reqValToken = Remote.getCookieValue('RequestVerificationToken');
            let p = fetch(url, {
                credentials: 'same-origin', method: 'post', body: JSON.stringify(data), headers: {
                    'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'X-CSRF-TOKEN': token, 'RequestVerificationToken': reqValToken
                }
            });
            p.then((response) => {
                Remote.checkSessionExpired(response);
            });
            return p;
        });
    }
    static postPlainFormData(url, data, callback, error) {
        let token = Remote.getCookieValue('X-CSRF-TOKEN');
        let reqValToken = Remote.getCookieValue('RequestVerificationToken');
        fetch(url, {
            credentials: 'same-origin', method: 'post', body: data, headers: {
                'X-CSRF-TOKEN': token, 'RequestVerificationToken': reqValToken
            }
        })
            .then((response) => {
            if (Remote.checkSessionExpired(response))
                return response;
            else
                return { error: true, statusText: response.statusText, originalMessage: response };
        })
            .catch((reason) => { Remote.onError(reason, error); })
            .then((result) => {
            if (result.ok)
                callback(result);
            else {
                Remote.onError(result, error);
            }
        });
    }
    static postPlainFormDataAsync(url, data) {
        let token = Remote.getCookieValue('X-CSRF-TOKEN');
        let reqValToken = Remote.getCookieValue('RequestVerificationToken');
        let p = fetch(url, {
            credentials: 'same-origin', method: 'post', body: data, headers: {
                'X-CSRF-TOKEN': token, 'RequestVerificationToken': reqValToken
            }
        });
        p.then((response) => {
            Remote.checkSessionExpired(response);
        });
        return p;
    }
    static postDataText(url, data, callback, error) {
        let token = Remote.getCookieValue('X-CSRF-TOKEN');
        let reqValToken = Remote.getCookieValue('RequestVerificationToken');
        fetch(url, {
            credentials: 'same-origin', method: 'post', body: JSON.stringify(data), headers: {
                'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'X-CSRF-TOKEN': token, 'RequestVerificationToken': reqValToken
            }
        })
            .then((response) => {
            if (Remote.checkSessionExpired(response))
                return response.text();
            else
                return { error: true, statusText: response.statusText, originalMessage: response };
        })
            .catch((reason) => { Remote.onError(reason, error); })
            .then((result) => {
            if (!result.error)
                callback(result);
            else {
                Remote.onError(result, error);
            }
        });
    }
    static get(url, callback, error) {
        var dt = new Date().getTime();
        var url = url.indexOf("?") > 0 ? url + "&dt=" + dt : url + "?dt=" + dt;
        fetch(url, { credentials: 'same-origin', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache' } })
            .then((response) => {
            if (Remote.checkSessionExpired(response))
                return response.json();
            else
                throw response;
        })
            .catch((reason) => { Remote.onError(reason, error); })
            .then((result) => {
            if (result && !result.error && !result.message)
                callback(result);
            else {
                Remote.onError(result, error);
            }
        });
    }
    static getAsync(url) {
        var url;
        return __awaiter(this, void 0, void 0, function* () {
            var dt = new Date().getTime();
            url = url.indexOf("?") > 0 ? url + "&dt=" + dt : url + "?dt=" + dt;
            let res = yield fetch(url, { credentials: 'same-origin', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache' } });
            if (!res.ok) {
                if (res.status && res.status === 200 && res.url.toLowerCase().indexOf("/login/") == -1 && res.url.toLowerCase().indexOf("/login") >= 0) {
                    window.location.reload(true);
                }
            }
            return res;
        });
    }
    static getText(url, callback, error) {
        fetch(url, { credentials: 'same-origin', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache' } })
            .then((response) => {
            if (Remote.checkSessionExpired(response))
                return response.text();
            else
                return { error: true, statusText: response.statusText, originalMessage: response };
        })
            .catch((reason) => { Remote.onError(reason, error); })
            .then((result) => {
            if (!result.error)
                callback(result);
            else {
                Remote.onError(result, error);
            }
        });
    }
    static getTextAsync(url) {
        return __awaiter(this, void 0, void 0, function* () {
            let res = yield fetch(url, { credentials: 'same-origin', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Cache-Control': 'no-cache' } });
            if (!res.ok) {
                if (res.status && res.status === 200 && res.url.toLowerCase().indexOf("/login/") == -1 && res.url.toLowerCase().indexOf("/login") >= 0) {
                    window.location.reload(true);
                }
            }
            return res;
        });
    }
    static serializeToJson(serializer) {
        var data = serializer.serialize().split("&");
        var obj = {};
        for (var key in data) {
            if (obj[data[key].split("=")[0]]) {
                if (typeof (obj[data[key].split("=")[0]]) == "string") {
                    let val = obj[data[key].split("=")[0]];
                    obj[data[key].split("=")[0]] = [];
                    obj[data[key].split("=")[0]].push(val);
                }
                obj[data[key].split("=")[0]].push(decodeURIComponent(data[key].split("=")[1]));
            }
            else {
                obj[data[key].split("=")[0]] = decodeURIComponent(data[key].split("=")[1]);
            }
        }
        return obj;
    }
    static onError(data, callback) {
        let message = "";
        if (data && data.responseJSON && data.responseJSON.Message) {
            message += data.responseJSON.Message + "<br/>";
        }
        else if (data) {
            for (let i in data.responseJSON) {
                if (data.responseJSON[i] != undefined && data.responseJSON[i].errors != undefined && data.responseJSON[i].errors.length > 0) {
                    message += data.responseJSON[i].errors[0].errorMessage + "<br/>";
                }
            }
        }
        if (data && data.message && message === "") {
            message = data.message;
        }
        if (message == "" && data)
            message = data.statusText;
        callback(message);
    }
    static download(url, callback, error) {
        return $.fileDownload(url).done(callback).fail(error);
    }
    static parseErrorMessage(a) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let m = yield a.json();
                if (typeof m === 'string') {
                    return m;
                }
                if (m[""]) {
                    if (m[""]["errors"]) {
                        return m[""]["errors"][0]["errorMessage"];
                    }
                    else {
                        if (m[""][0] && typeof m[""][0] === "string") {
                            return m[""][0];
                        }
                    }
                }
                else {
                    return m["Message"];
                }
            }
            catch (e) {
                return a.statusText;
            }
        });
    }
    static getCookieValue(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1);
            if (c.indexOf(name) == 0)
                return c.substring(name.length, c.length);
        }
        return "";
    }
    static checkSessionExpired(res) {
        return __awaiter(this, void 0, void 0, function* () {
            if (res.status && res.status === 200 && res.url.toLowerCase().indexOf("/login/") == -1 && res.url.toLowerCase().indexOf("/login") >= 0) {
                window.location.reload(true);
                return false;
            }
            else if (res.status === 400 >= 0) {
                window.location.reload(true);
                return false;
            }
            return true;
        });
    }
}
//# sourceMappingURL=remote.js.map