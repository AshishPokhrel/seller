﻿import * as React from "react";
import * as ReactDOM from "react-dom";

import { LandingPage } from "./admin/landingPage";

ReactDOM.render(
    <LandingPage />, document.getElementById("landingPage")
);