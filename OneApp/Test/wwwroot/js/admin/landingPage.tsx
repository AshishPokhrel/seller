﻿import * as React from "react";
import * as ReactDOM from "react-dom";
import { NavBar } from "./navbar";
import { Layout } from "./layout";

interface ILandingPageProps {

}

interface ILandingPageState {

}

export class LandingPage extends React.Component<ILandingPageProps, ILandingPageState>{
    constructor(props: any) {
        super(props);
    }
    render() {
        return (
            <Layout>
            </Layout>
        )
    }
}