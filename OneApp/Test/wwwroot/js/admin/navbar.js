import * as React from "react";
export class NavBar extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (React.createElement("header", null,
            React.createElement("nav", { className: "navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3" },
                React.createElement("div", { className: "container" },
                    React.createElement("a", { className: "navbar-brand", "asp-area": "", "asp-page": "/Index" }, "Test"),
                    React.createElement("button", { className: "navbar-toggler", type: "button", "data-toggle": "collapse", "data-target": ".navbar-collapse", "aria-controls": "navbarSupportedContent", "aria-expanded": "false", "aria-label": "Toggle navigation" },
                        React.createElement("span", { className: "navbar-toggler-icon" })),
                    React.createElement("div", { className: "navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse" },
                        React.createElement("ul", { className: "navbar-nav flex-grow-1" },
                            React.createElement("li", { className: "nav-item" },
                                React.createElement("a", { className: "nav-link text-dark", "asp-area": "", "asp-page": "/Index" }, "Home")),
                            React.createElement("li", { className: "nav-item" },
                                React.createElement("a", { className: "nav-link text-dark", "asp-area": "", "asp-page": "/Privacy" }, "Privacy"))))))));
    }
}
//# sourceMappingURL=navbar.js.map