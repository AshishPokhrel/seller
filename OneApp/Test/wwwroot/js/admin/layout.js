import * as React from "react";
import { Container } from 'reactstrap';
import { NavBar } from './navbar';
export class Layout extends React.Component {
    render() {
        return (React.createElement("div", null,
            React.createElement(NavBar, null),
            React.createElement(Container, null, this.props.children)));
    }
}
//# sourceMappingURL=layout.js.map