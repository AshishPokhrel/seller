﻿import * as React from "react";
import { Container } from 'reactstrap';
import { NavBar } from './navbar';


interface ILayoutProps {

}

interface ILayoutState {

}

export class Layout extends React.Component<ILayoutProps, ILayoutState>{
    render() {
        return (
            <div>
                <NavBar />
                <Container>
                    {this.props.children}
                </Container>
            </div>
        );
    }
}
