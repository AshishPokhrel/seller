﻿using Dapper;
using Infrastructure.DatabaseContext;
using Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class SystemRepository : ISystemInterface
    {
        private readonly IDatabaseContext _context;

        public SystemRepository(IDatabaseContext context)
        {
            _context = context;
        }

        public Task<NavbarModel> getById()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<NavbarModel>> getAll()
        {
            var sql = $@"SELECT * FROM Navbar";
            var data = await _context.Connection.QueryAsync<NavbarModel>(sql);
            return data;
        }
    }
}
