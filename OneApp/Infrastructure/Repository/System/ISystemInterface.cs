﻿using Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
   public interface ISystemInterface
    {
        Task<IEnumerable<NavbarModel>> getAll();
        Task<NavbarModel> getById();
    }
}
