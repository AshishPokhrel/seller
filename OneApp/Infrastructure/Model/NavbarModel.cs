﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Model
{
    public class NavbarModel
    {
        public int Id { get; set; }
        public string MasterCompanyId { get; set; }
        public string Title { get; set; }
        public string ColorName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
