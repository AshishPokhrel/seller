﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Infrastructure.DatabaseContext
{
    public interface IDatabaseContext : IDisposable
    {
        string ConnectionString { get; set; }
        IDbConnection Connection { get; }
    }
}
